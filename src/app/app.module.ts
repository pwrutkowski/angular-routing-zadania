import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { GreetingComponent } from './greeting/greeting.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [{
  path: 'welcome/:name',
  component: GreetingComponent
}];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    GreetingComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
