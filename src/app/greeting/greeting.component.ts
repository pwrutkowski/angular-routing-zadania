import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-greeting',
  templateUrl: './greeting.component.html',
})
export class GreetingComponent implements OnInit {
  name: string;

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute
      .params
      .subscribe(params => this.name = params.name);
  }

}


// 4.Utwórz komponent o nazwie CarDetailsComponent.
// Do menu dodaj linki do:
// car-details/1
// car-details/2
// car-details/5
//
// Komponent CarDetailsComponent subskrybuje zmianę parametrów ścieżki. Gdy brak parametru wystepującego w ścieżce po slashu,
// powinno nastąpić przekierowanie pod ścieżkę ''.
// Gdy ten parametr istnieje, szablon wypisze "Dane samochodu <tu wartość odczytana ze ścieżki>".
// 5. Utwórz interfejs Car (id, speed, make, model). Komponent CarDetailsComponent posiada pole typu Car.
// Przy zmianie parametru ścieżki, gdy id istnieje w ścieżce (liczba ze ścieżki), do pola przypisze przykładowy obiekt,
// którego wartości kazdego pola wykorzystają wartość id ze ścieżki. Szablon wypisze dane tego samochodu w postaci tabeli.
//  */
